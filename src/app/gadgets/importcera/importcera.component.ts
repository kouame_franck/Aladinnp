import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { LocalService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');

@Component({
  selector: 'app-importcera',
  templateUrl: './importcera.component.html',
  styleUrls: ['./importcera.component.scss']
})
export class ImportceraComponent implements OnInit {
  @Input() url:any
  err=""
  errfile=""
  errfile4=""
  error=""
  
  @Output() changecomponent=new EventEmitter<boolean>();
  @Input() stylo:boolean=false
  @Input() tasse:boolean=false
  @Input() portcle:boolean=false
  file2:any
  file3:any
  file4:any
  imagepreview:any
  viewimage:any
  ViewImg:any
  oui=false
  er=true
  ert=true
  qnte:any=50
  qtyunit:any
  quantitetasse:any=1
  Price:any
  publicprice=150
  seriprice=150
  trftprice=100
  uvprice=350
  priceporte=300
  t:any
  magprice=4000
  ordiprice=3000
  //
  pub=false
  serie=false
  transfert=false
  uv=false
  porte=false
  magique=false
  ordinaire=false
  constructor(private localservice:LocalService) { }

  ngOnInit(): void {
  }
  //import the images
  Uplode(event:any){
    this.file2 =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(this.file2);
    console.log(event)
     
   }
  
  Uplade(event:any){
    this.file3 =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.viewimage= reader.result;
   
   };
   
   reader.readAsDataURL(this.file3);
    console.log(event)
     
   }
   Upladeimage(event:any){
    this.file4 =event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
  
   this.ViewImg= reader.result;
   
   };
   
   reader.readAsDataURL(this.file4);
    console.log(event)
     
   }
   showoui(){
     this.oui=!this.oui
   }

   //quantite
   qtyplus(){
     this.qnte=this.qnte + 50
   }

   qtyminus(){
     if(this.qnte>50){
      this.qnte=this.qnte - 50
     }else{
      this.qnte=this.qnte
     }
   }
//show price
  showpblicprice(){
    this.Price=this.publicprice
    this.ert=false
    this.pub=true
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=false
  }
 showseriprice(){
   this.er=false
   this.Price= this.publicprice + this.seriprice 
   this.pub=true
    this.serie=true
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=false
 }

 showtrftprice(){
  this.Price= this.publicprice + this.trftprice
  this.er=false
  this.pub=true
    this.serie=false
    this.transfert=true
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=false
}
showuvprice(){
  this.er=false
  this.Price= this.publicprice + this.uvprice 
  this.pub=true
    this.serie=false
    this.transfert=false
    this.uv=true
    this.porte=false
    this.magique=false
    this.ordinaire=false
}
showporteprice(){
  this.Price=this.priceporte
  this.pub=false
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=true
    this.magique=false
    this.ordinaire=false
}
showmagprice(){
  this.Price=this.magprice
  this.pub=false
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=true
    this.ordinaire=false
}
showordiprice(){
  this.Price=this.ordiprice
  this.pub=false
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=true
}

addtocart=()=>{
  let cart: any
  if(this.stylo || this.portcle){
   this.qtyunit=this.qnte
  }
  if(this.tasse){
    this.qtyunit=this.quantitetasse
  }
if(this.file2!=undefined && this.file4!=undefined){
 cart={
   type_product:"crea",
   category:"gadget",
   qty:this.qtyunit,
   t:this.Price * this.qtyunit,
   price:this.Price,
   face1: this.url,
   face2:this.imagepreview ,
   f3: this.viewimage,
   f4: this.ViewImg
 }
 if(this.tasse){
   Object.assign(cart ,{
     type:"Tasse"
   })
 }
 if(this.stylo){
  Object.assign(cart ,{
    type:"Stylo"
  })
}
if(this.portcle){
  Object.assign(cart ,{
    type:"Porte-clé"
  })
}
if(this.pub){
  Object.assign(cart, {
    nom:"Stylo grand public",
    prixe:this.publicprice
  })
}
if(this.serie){
  Object.assign(cart, {
    impr:"Sérigraphie",
    prix:this.seriprice
  })
}
if(this.transfert){
  Object.assign(cart, {
    impr:"Transfert",
    prix:this.trftprice
  })
}
if(this.uv){
  Object.assign(cart, {
    impr:"UV",
    prix:this.uvprice
  })
}
if(this.porte){
  Object.assign(cart, {
    impr:"Plastique",
    prix:this.priceporte
  })
}
if(this.ordinaire){
  Object.assign(cart, {
    impr:"Tasse ordinaire",
    prix:this.ordiprice
  })
}
if(this.magique){
  Object.assign(cart, {
    impr:"Tasse magique",
    prix:this.magprice
  })
}
}else{
  cart={
    type_product:"crea",
    category:"gadget",
    qty:this.qtyunit,
    t:this.Price * this.qtyunit,
    price:this.Price,
    face1: this.url,
    face2:null ,
    f3: this.viewimage,
    f4: null
  }
  if(this.tasse){
    Object.assign(cart ,{
      type:"Tasse"
    })
  }
  if(this.stylo){
   Object.assign(cart ,{
     type:"Stylo"
   })
 }
 if(this.portcle){
   Object.assign(cart ,{
     type:"Porte-clé"
   })
 }
 if(this.pub){
   Object.assign(cart, {
     nom:"Stylo grand public",
     prixe:this.publicprice
   })
 }
 if(this.serie){
   Object.assign(cart, {
     impr:"Sérigraphie",
     prix:this.seriprice
   })
 }
 if(this.transfert){
   Object.assign(cart, {
     impr:"Transfert",
     prix:this.trftprice
   })
 }
 if(this.uv){
   Object.assign(cart, {
     impr:"UV",
     prix:this.uvprice
   })
 }
 if(this.porte){
   Object.assign(cart, {
     impr:"Plastique",
     prix:this.priceporte
   })
 }
 if(this.ordinaire){
   Object.assign(cart, {
     impr:"Tasse ordinaire",
     prix:this.ordiprice
   })
 }
 if(this.magique){
   Object.assign(cart, {
     impr:"Tasse magique",
     prix:this.magprice
   })
 }
}

try {
  if((this.pub || this.serie || this.transfert || this.uv || this.porte || this.ordinaire || this.magique) && this.file3!=undefined){
    this.localservice.adtocart(cart);
    myalert.fire({
      title:'<strong>produit ajouté</strong>',
      icon:'success',
      html:
        '<h6 style="color:blue">Felicitation</h6> ' +
        '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
        '<a href="/cart">Je consulte mon panier</a>' 
        ,
      showCloseButton: true,
      focusConfirm: false,
     
    })
  }
  if(this.pub==false){
    this.error="choisisez le type de stylo"
  }
  if(this.serie==false && this.transfert==false && this.uv==false){
      this.err="choisissez un type d'impression"
    }
  if(this.oui==true ){
    if(this.file2==undefined){
      this.errfile="Veillez importer la face arrière du visuel"
    }
    if(this.file4==undefined && this.file3==undefined){
      this.errfile4="Veillez importer la face avant et arrière de la maquette"
    }
  }else{
    if(this.file3==undefined){
      this.errfile4="importer la maquette de la face avant"
    }
  
  }
  if(this.porte==false){
    this.err="choissisez le type de porte-clé"
  }
  if(this.ordinaire==false && this.magique==false){
    this.err="choisissez un type de tasse"
  }
} catch (error) {
  console.log(error)
}
 console.log(cart, this.pub)
}
}

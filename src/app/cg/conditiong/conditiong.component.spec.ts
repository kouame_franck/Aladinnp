import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditiongComponent } from './conditiong.component';

describe('ConditiongComponent', () => {
  let component: ConditiongComponent;
  let fixture: ComponentFixture<ConditiongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConditiongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditiongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

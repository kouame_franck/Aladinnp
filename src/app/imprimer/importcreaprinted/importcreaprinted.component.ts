import { Component, OnInit,Input } from '@angular/core';
import { elementAt } from 'rxjs/operators';
import { LocalService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
@Component({
  selector: 'app-importcreaprinted',
  templateUrl: './importcreaprinted.component.html',
  styleUrls: ['./importcreaprinted.component.scss']
})
export class ImportcreaprintedComponent implements OnInit {
Price:any
recu:any
error="";
err=""
erreur=""
promo=false
promo1=true
A42S=false
A43S=false
A44S=false
A52S=false
A53S=false
A54S=false
A62S=false
A63S=false
A64S=false
@Input() url: any
priceSD=3000
priceD=4000
largeur:any
longueur: any
quantite:any=2
hideetiquette=false
carnet=false
file3:any
viewimage:any
priceA4_2S=6500
newprice:any
priceA4_3S=9000
priceA4_4S=11500

priceA5_2S=3800
priceA5_3S=5200
priceA5_4S=6600

priceA6_2S=2500
priceA6_3S=2500
priceA6_4S=2500

etiquette=false
carnete=false
sansdecouper=false
avecdecoupe=false
  constructor(private localservice:LocalService) { }

  ngOnInit(): void {
  }
ShowpriceSD(){
  this.Price=this.priceSD
  this.sansdecouper=true
  this.avecdecoupe=false
  this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=false
  }
ShowpriceD(){
    this.Price=this.priceD
    this.sansdecouper=false
    this.avecdecoupe=true
    this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=false
  }
showhideetiquette(){
  this.hideetiquette=true
  this.carnet=false
  this.carnete=false
  this.etiquette=true
}
showcarnet(){
  this.carnete=true
  this.hideetiquette=false
  this.carnet=true
  this.etiquette=false
}
Uplade(event:any){
  this.file3 =event.target.files[0]
  const reader = new FileReader();
reader.onload = () => {

 this.viewimage= reader.result;
 
 };

 reader.readAsDataURL(this.file3);

   
 }

 //show price format A4

 showpriceA4_2S(){
   this.Price=this.priceA4_2S
   this.newprice= this.priceA4_2S - (1500)
   this.promo=false
   this.promo1=true
   this.A42S=true
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
 }
 showpriceA4_3S(){
  this.Price=this.priceA4_3S
  this.promo=true
  this.promo1=false
  this.A42S=false
   this.A43S=true
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
}
showpriceA4_4S(){
  this.Price=this.priceA4_4S
  this.promo=true
  this.promo1=false
  this.A42S=false
   this.A43S=false
   this.A44S=true
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
}


// show price format A5
showpriceA5_2S(){
  this.Price=this.priceA5_2S
  this.newprice= this.priceA5_2S -(1300)
  this.promo=false
  this.promo1=true
  this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=true
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
}
showpriceA5_3S(){
 this.Price=this.priceA5_3S
 this.promo=true
  this.promo1=false
 this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=true
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
}
showpriceA5_4S(){
 this.Price=this.priceA5_4S
 this.promo=true
  this.promo1=false
 this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=true
   this.A62S=false
   this.A63S=false
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
}

// show price format A6
showpriceA6_2S(){
  this.Price=this.priceA6_2S
  this.newprice= this.priceA6_2S -(700)
  this.promo=false
  this.promo1=true
  this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=true
   this.A63S=false
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
}
showpriceA6_3S(){
 this.Price=this.priceA6_3S
 this.promo=true
  this.promo1=false
 this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=true
   this.A64S=false
   this.avecdecoupe=false
   this.sansdecouper=false
}
showpriceA6_4S(){
  this.promo=true
  this.promo1=false
 this.Price=this.priceA6_4S
   this.A42S=false
   this.A43S=false
   this.A44S=false
   this.A52S=false
   this.A53S=false
   this.A54S=false
   this.A62S=false
   this.A63S=false
   this.A64S=true
   this.avecdecoupe=false
   this.sansdecouper=false
}
addcart=()=>{
let cart:any
if(this.A42S || this.A52S || this.A62S){
  this.Price=this.newprice
}else{
  this.Price=this.Price
}

cart ={ 
  type_product:"crea",
  t:+this.Price * parseInt(this.quantite),
  category:"imprimer",
  face1:this.url,
  f3:this.viewimage,
  qty: this.quantite,
  price:this.Price
 }
 if(this.etiquette){
   Object.assign(cart, {
    
       type:"etiquette",
       longueur: this.longueur,
       largeur:this.largeur,
      
    
   })
 }
 if(this.carnete){
  Object.assign(cart, {
    
      type:"Carnet",
      numero:this.recu,
     
    
  })
}
if(this.sansdecouper){
  Object.assign(cart, {
  
      nom:"A4 sans decoupe",
      prix: this.priceSD
    
  })
}
if(this.avecdecoupe){
  Object.assign(cart, {
   
      nom:"A4 avec decoupe",
      prix: this.priceD
   
  })
}
if(this.A42S){
  Object.assign(cart, {
    
      nom:"A4 + 2 souches",
      prix: this.priceA4_2S,
      newp: this.priceA4_2S - 1500
    
  })
}
if(this.A43S){
  Object.assign(cart, {
  
      nom:"A4 + 3 souches",
      prix: this.priceA4_3S
    
  })
}
if(this.A44S){
  Object.assign(cart, {
  
      nom:"A4 + 4 souches",
      prix: this.priceA4_4S
    
  })
}
if(this.A52S){
  Object.assign(cart, {
  
      nom:"A5 + 2 souches",
      prix: this.priceA5_2S,
      newp: this.priceA5_2S - 1300
    
  })
}
if(this.A53S){
  Object.assign(cart, {
   
      nom:"A5 + 3 souches",
      prix: this.priceA5_3S
    
  })
}
if(this.A54S){
  Object.assign(cart, {
 
      nom:"A5 + 4 souches",
      prix: this.priceA5_4S
    
  })
}
if(this.A62S){
  Object.assign(cart, {
  
      nom:"A6 + 2 souches",
      prix:this.priceA6_2S,
      newp: this.priceA6_2S - 700
  })
}
if(this.A63S){
  Object.assign(cart, {
    
      nom:"A6 + 3 souches",
      prix: this.priceA6_3S
    
  })
}
if(this.A64S){
  Object.assign(cart, {
    
      nom:"A6 + 4 souches",
      prix: this.priceA6_4S
    
  })
}
try{
  if((this.etiquette || this.carnete) && (this.file3!=undefined)){
    if(this.A42S || this.A43S || this.A44S || this.A52S || this.A53S || this.A54S || this.A62S || this.A63S || this.A64S || this.avecdecoupe || this.sansdecouper){
      this.localservice.adtocart(cart);
      myalert.fire({
        title:'<strong>produit ajouté</strong>',
        icon:'success',
        html:
          '<h6 style="color:blue">Felicitation</h6> ' +
          '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
          '<a href="/cart">Je consulte mon panier</a>' 
          ,
        showCloseButton: true,
        focusConfirm: false,
       
      })
       console.log(cart)
     console.log(cart)
    }else{
      this.erreur="Veuillez choisir un format"
    }
    
  }
  if(this.file3==undefined){
     this.error="entrer votre marquette "
     
  }
   if(this.etiquette==false && this.carnete==false){
    this.err="choissisez le type d'imprimé"
   }

}catch(e:any){
  console.log(e)
}

}
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header3',
  templateUrl: './header3.component.html',
  styleUrls: ['./header3.component.scss']
})
export class Header3Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  goTo(event:Event){
    window.location.href='/partners';
  }
View(){
  let view=document.getElementById('fluid')
  view?.scrollIntoView({behavior:'smooth'})
}
}

import { Component, OnInit ,Input,Output,EventEmitter,OnChanges} from '@angular/core';
import { AladinService } from 'src/app/core';
@Component({
  selector: 'app-aladin-faces',
  templateUrl: './aladin-faces.component.html',
  styleUrls: ['./aladin-faces.component.scss']
})
export class AladinFacesComponent implements OnInit ,OnChanges{
  Front="imgfront";
  Back="imgback";

@Input() URL:any;
@Input() face1:any;
@Input() back:any;
@Input() front:any;
@Output() newItemEvent =new EventEmitter<string>();

  constructor(private aladin:AladinService) { }

  ngOnInit(): void {
    this.aladin.faceupdated.subscribe(urls=>{
      if(urls){
        this.face1=urls.face1;
        this.URL=urls.face2;
        console.log(this.face1,this.URL);
      }else{
        console.log(urls)
      }
    
  
    })
  }

ngOnChanges(){
  

}

  addNewItem(value: any) {
    this.newItemEvent.emit(value);
  }
}

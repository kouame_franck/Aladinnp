  import { Component, Input, OnInit,OnChanges,Output,EventEmitter,OnDestroy, SimpleChanges} from '@angular/core';
  import { Subscription } from 'rxjs';
  import { CartService, LocalService } from 'src/app/core';
  import { charAtIndex } from 'pdf-lib';

  declare  var require:any;
  var myalert=require('sweetalert2');
  var $ = require("jquery");
  @Component({
    selector: 'app-aladin-designs-packs',
    templateUrl: './aladin-designs-packs.component.html',
    styleUrls: ['./aladin-designs-packs.component.scss'],
  })

  export class AladinDesignsPacksComponent implements OnInit,OnChanges{
  data:any=[];
  price:any=0;
  unit_price=0;
  @Input() face1:any;
  @Input() face2:any;
  qty:any=100;
  subscription:any=Subscription
  mydesigns:any[]=[];
  inpnmb =1;
  prod:any=[];
  active_prod:any;
  hide_calco=true;
  show_modal=true;
  active=false;
  newdata:any =[];
  reste="";
  price_1=[5000,5000,7000,7000,10000,15000,15000];
  // size=["20/30","30/30","33/40","40/45","50/60"];
  size_sac=["15/20","20/25","25/30","28/35","35/45","45/50","50/55"];
  size:any={
    s1:false,
    s2:false,
    s3:false,
    s4:false,
    s5:false,
    s6:false,
  }
  type_impr:any={
    seri:true
  }

  cart:any;
  product:any

    constructor(private local:LocalService,private service:CartService) {

    
    }

    ngOnInit(): void {
    this.Initdata()
    this.Updatedata()
      
    $("body").children().first().before($(".modal"));
    
    }

    decremet_by(){
      let price=this.unit_price
      this.inpnmb =(+this.inpnmb)*2;
      if(this.inpnmb>0&&price!=null){
        this.price= this.inpnmb*(+price);
        this.qty=this.qty*2
      }
    }

    ngOnChanges():void{  
      
    
    }


    Initdata=()=>{
      this.mydesigns=this.local.items;
      if(this.mydesigns.length>0){
        if(this.data.length>0){
          this.data=[]
        }  
        for(let item of this.mydesigns){
        if(item.data.category=="packs"){
          this.data.push(item);

        }
      
        }
      }
      console.log(this.data.length);
    }



    Updatedata=()=>{
      this.service.dignUpdated.subscribe(message=>{
        this.mydesigns=this.local.items;
        if(message>0){
            this.data=[];
            for(let item of this.mydesigns){

            if(item.data.category==="packs"){
              this.data.push(item);
      
            }else{
              console.log(item.data.category,"kluhiu")
            }
          
            }
          
        }

      })
    
    }

    getProdprice=(event:any)=>{
      let id=event.target.id
      this.cart = charAtIndex(id,0);
      let product= charAtIndex(id,2)
      this.unit_price=parseInt(event.target.name)
      this.price=this.unit_price
      this.inpnmb=1;
      this.active_prod= product[0];
      this.getId(this.active_prod);
    
    }


  getPrice(){
    let price=this.unit_price;
    this.inpnmb =+this.inpnmb+1;
      if(this.inpnmb>0&&price!=null){
        this.price= this.inpnmb*(+price);
      }
      this.qty=this.inpnmb*100;
    }


    
    removeItem(id:any){
      if(id!=-1){
        this.local.removeOne(id);
      }
      
    }

    addtocart=()=>{
      if(this.size.s1){
        let cart: any
      let data = this.getId(+this.active_prod);
      cart={
        t:this.price,
        price:this.unit_price,
        qty:this.inpnmb,
        size:this.size,
        type:this.type_impr,
        name:data.data.name,
        f1:data.face1,
        f2:data.face2,
        production_f1:data.production_f1,
        production_f2:data.production_f2,
        type_product:"editor",
          category:"packs"

      }
        this.local.adtocart(cart);
        myalert.fire({
        title:'<strong>produit ajouté</strong>',
        icon:'success',
        html:
          '<h6 style="color:blue">Felicitation</h6> ' +
          '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
          '<a href="/cart">Je consulte mon panier</a>' 
          ,
        showCloseButton: true,
        focusConfirm: false,
      
      })

      this.data.splice(+this.active_prod,1);
      this.removeItem(this.cart[0]);

      if(this.data.length==0){
        setTimeout(()=>{
          location.reload()

        },1000)

      }

      }else{
        myalert.fire({
          title:'<strong>warning</strong>',
          icon:'error',
          html:
            '<h6 style="color:red">choisissez la taille svp</h6> ' +
           
            '<a href="/cart">Je consulte mon panier</a>' 
            ,
          showCloseButton: true,
          focusConfirm: false,
        
        })

      }
      
    }



    getId=(id:any)=>{
      if(id!=-1){
        return this.data[+id];
      }else{
        return -1;
      }
    
    }

    removeOneItem(event:any){
      let id=event.target.id;
      console.log(id)
      let cart = charAtIndex(id,2);
      let prod=charAtIndex(id,0);
      console.log(prod[0])
      if(+cart[0]!= -1){
        this.data.splice(+prod[0],1);

        this.local.removeOne(cart[0]);
      }
      
  }


    hidemodal(){
      this.hide_calco=!this.hide_calco;
      console.log(this.size);
    }

    divide(){
      let price=this.unit_price
      if(this.inpnmb >5){
        this.inpnmb =~~((+this.inpnmb)/2);
        if(this.inpnmb> 100 && price!=null){
          this.price= this.inpnmb*(+price);
        }
      }
    }

    getPricem(){
      let price=this.unit_price
      this.inpnmb=this.inpnmb-1;
      if(this.inpnmb>0 && price!=null){
        if(((+this.inpnmb)*(+this.price))>0){
          this.price= ((+this.inpnmb)*(+price));
        }
        this.qty=this.qty-100
    }else{
      this.inpnmb=this.inpnmb+1;

    }
  }



      OnsizeChange(event:any){
    if(event.target.value!=="value"){
      this.size.s1=true;
    }
  }


  }
